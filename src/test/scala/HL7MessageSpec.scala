import java.nio.ByteOrder

import akka.util.ByteString
import model.protocol.{HL7Mess, MLLP}
import org.scalatest.FlatSpec

class HL7MessageSpec extends FlatSpec {

  val msgStr = """MSH|^~\&|SENDING_APPLICATION|SENDING_FACILITY|RECEIVING_APPLICATION|RECEIVING_FACILITY|20110613083617||ADT^A01|934576120110613083617|P|2.3||||
                 |EVN|A01|20110613083617|||
                 |PID|1||135769||MOUSE^MICKEY^||19281118|M|||123 Main St.^^Lake Buena Vista^FL^32830||(407)939-1289^^^theMainMouse@disney.com|||||1719|99999999||||||||||||||||||||
                 |PV1|1|O|||||^^^^^^^^|^^^^^^^^""".stripMargin

  "A Message" should "decode from string and encode in the same string" in {
    val mess:HL7Mess = HL7Mess.decode(msgStr)
    assert(mess.mshSegment().get.messageControlID().equals("934576120110613083617"))

    val encoded = HL7Mess.encode(mess)
    assert(encoded.equals(msgStr))
  }

  implicit val byteOrder = ByteOrder.BIG_ENDIAN

  val  testmsg = "test msg 2342348f9gf7df7v d734"
  val b:ByteString = ByteString.createBuilder.putByte(11).append(ByteString(testmsg)).putByte(12).putByte(13).result()

  "A MLLP Request" should "decode in testmsg string" in {
    val decoded = MLLP.decode(b);
    assert(decoded.get.equals(testmsg))
  }

  "A MLLP Request" should "encode in the same ByteString" in {
    val encoded = MLLP.encode(testmsg);
    assert(encoded.sameElements(b))
  }

}
