import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.typesafe.config.{Config, ConfigFactory}
import model.protocol._

import scala.util.{Failure, Success, Try}

case class Response(message: String)

class SimplisticHandler extends Actor with ActorLogging {

  log.info("SimplisticHandler Actor started")

  import Tcp._

  def receive = {
    case Received(data) =>

      Try(randomAnswer(data)) match {
        case Success(answer) => sender() ! Write(answer)
        case Failure(f) => sender() ! Write(ByteString("Some went wrong"))
      }
      /*sender() ! Write(ByteString("""HTTP/1.1 200 OK
                                    |Server: akka-http/10.1.1
                                    |Date: Mon, 09 Jul 2018 17:05:24 GMT
                                    |Content-Type: application/json
                                    |Content-Length: 56
                                    |
                                    |{"id":7,"text":"First message text 111111","sent":false}""".stripMargin))*/
    case PeerClosed =>
      log.info("Peer closed")
      context.stop(self)
  }

  def randomAnswer(in:ByteString): ByteString ={
    val mess:HL7Mess = HL7Mess.decode(MLLP.decode(in).getOrElse(""))
    //MSH|^~\&|CATH|StJohn|AcmeHIS|StJohn|20061019172719||ACK^O01|<MESSAGE CONTROL ID>|P|2.3 MSA|AE|MSGID12349876 ERR|^^^207&ERROR&hl70357&&errmsg
    val mshSegment:Option[HL7MSHSegment] = mess.mshSegment()
    val msh = HL7MSHSegment(List(
      HL7Composite("^~\\&"),
      HL7Composite("CATH"),
      HL7Composite("StJohn"),
      HL7Composite("AcmeHIS"),
      HL7Composite("StJohn"),
      HL7Composite("20061019172719"),
      HL7Composite(""),
      HL7Composite("ACK^O01"),
      HL7Composite(if(mshSegment.isDefined) mshSegment.get.messageControlID() else ""),
      HL7Composite("P"),
      HL7Composite("2.3 MSA"),
      HL7Composite("AE"),
      HL7Composite("MSGID12349876")
    ))
    val err = HL7ERRSegment(List(
      HL7Composite("^^^207&ERROR&hl70357&&errmsg")
    ))
    val response:HL7Mess = if(Math.random()*2>1) HL7Mess(List(msh, err)) else HL7Mess(List(msh))
    MLLP.encode(HL7Mess.encode(response))
  }
}

class Server extends Actor with ActorLogging {

  import Tcp._
  import context.system

  val config: Config = ConfigFactory.load()

  IO(Tcp) ! Bind(self, new InetSocketAddress(config.getString("randomAnswerServer.host"), config.getInt("randomAnswerServer.port")))

  def receive = {
    case b @ Bound(localAddress) =>
      log.info("Bound")

    case CommandFailed(_: Bind) =>
      log.info("Command failed.")
      context.stop(self)

    case c @ Connected(remote, local) =>
      log.info("Connected with hostname: " + remote.getHostName)
      val handler = context.actorOf(Props[SimplisticHandler])
      val connection = sender()
      connection ! Register(handler)
  }
}

object MLLPRandomAnswerServer extends App {
  val system = ActorSystem("mllpServer")
  val server = system.actorOf(Props[Server])
}