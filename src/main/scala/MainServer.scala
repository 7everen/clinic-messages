import akka.http.scaladsl.Http

import scala.util.{Failure, Success}

object MainServer extends App {
  val modules = new AllModules

  import modules._

  Http().bindAndHandle(modules.endpoints.routes, modules.config.getString("mainServer.host"), modules.config.getInt("mainServer.port")).onComplete {
    case Success(b) => system.log.info(s"application is up and running at ${b.localAddress.getHostName}:${b.localAddress.getPort}")
    case Failure(e) => system.log.error(s"could not start application: {}", e.getMessage)
  }
}