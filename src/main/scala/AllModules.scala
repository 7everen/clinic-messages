import java.util.concurrent.Executors

import actor.ScheduleActor
import akka.actor.{ActorSystem, Props}
import akka.stream._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import endpoints.Endpoints
import repository.MessageRepository
import scalikejdbc.{AutoSession, ConnectionPool, ConnectionPoolSettings, DBSession}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class AllModules {
  val logger = Logger[AllModules]
  logger.info("All Modules init")

  implicit lazy val system = ActorSystem("mainServer")
  implicit lazy val materializer = ActorMaterializer()
  implicit lazy val executor: ExecutionContext = system.dispatcher

  lazy val config: Config = ConfigFactory.load()

  Class.forName(config.getString("jdbc.driver"))
  val maxConnections = config.getInt("jdbc.maxConnections");

  ConnectionPool.singleton(
    config.getString("jdbc.url"),
    config.getString("jdbc.username"),
    config.getString("jdbc.password"),
    ConnectionPoolSettings(
      initialSize = 1,
      maxSize = maxConnections
    ))

  lazy val session: DBSession = AutoSession
  lazy val databaseExecutorContext: ExecutionContext =
    ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(maxConnections))

  lazy val messageRepository = new MessageRepository(session, databaseExecutorContext)
  lazy val endpoints = new Endpoints(messageRepository)(databaseExecutorContext, materializer)

  val scheduleActor = system.actorOf(Props(classOf[ScheduleActor], messageRepository, config, materializer))
  val repeatTime = config.getInt("mainServer.scheduleTimeRepeat")
  val cancellable =
    system.scheduler.schedule(
      repeatTime milliseconds,
      repeatTime seconds,
      scheduleActor,
      ScheduleActor.CHECK_MESSAGE)
}


/*val connection = Tcp().outgoingConnection(config.getString("client.address"), config.getInt("client.port"))
val sink = Sink.ignore
val source = Source.actorRef(1000, OverflowStrategy.fail)
val tlsConnectionFlow = tlsStage(TLSRole.client).join(connection)
val sourceActor = tlsConnectionFlow.join(logging).to(sink).runWith(source)*/