package actor

import actor.MessageDataActor._
import akka.actor.Actor
import repository.MessageRepository

import scala.concurrent.ExecutionContext

class MessageDataActor(messageRepo: MessageRepository) extends Actor {

  implicit val system = context.system
  implicit val ec: ExecutionContext = system.dispatcher

  override def receive: Receive = {
    case up: UpdateMessage => {
      messageRepo.findFirstDidntSend();
    }
  }

}

object MessageDataActor {
  case class CreatePerson(fullName: String, phone: Option[String])
  case class GetPerson(id: Int)
  case class UpdateMessage(id: Long, sent: Boolean)
  case class DeletePerson(id: Int)
}
