package actor

import akka.NotUsed
import akka.actor.Actor
import akka.stream.TLSProtocol.NegotiateNewSession
import akka.stream.{ActorMaterializer, OverflowStrategy, TLSProtocol, TLSRole}
import akka.stream.scaladsl.{BidiFlow, Flow, Sink, Source, TLS, Tcp}
import akka.util.ByteString
import com.typesafe.scalalogging.Logger
import javax.net.ssl.SSLContext

import repository.MessageRepository

import scala.concurrent.{ExecutionContext}
import scala.util.Success
import com.typesafe.config.Config
import model.protocol.{HL7Mess, MLLP}

class ScheduleActor(repository: MessageRepository, config: Config, materializer: ActorMaterializer) extends Actor {

  val logger = Logger[ScheduleActor]
  implicit val system = context.system
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val m: ActorMaterializer = materializer

  override def receive: Receive = {
    case ScheduleActor.CHECK_MESSAGE => {
      logger.info("Check message")
      repository.findFirstDidntSend().onComplete{
        case Success(Some(value)) => {
          val source = Source.actorRef(1000, OverflowStrategy.fail)
          val connection = Tcp().outgoingConnection(config.getString("randomAnswerServer.host"), config.getInt("randomAnswerServer.port"))

          val flow  = if(config.getBoolean("randomAnswerServer.tls")) tlsClientLayer.join(connection) else connection

          val srcActor = flow.join(bidiFlow).to(Sink.ignore).runWith(source)

          val msh = HL7Mess.decode(value.text).mshSegment()
          val hl7Id = if(msh.isDefined) msh.get.messageControlID() else ""
          logger.info(s"Found message for sending with ID: ${value.id} with HL7 ID: $hl7Id sent to Random Answer Server." )
          srcActor ! MLLP.encode(value.text)

        }
        case _ => logger.info("No messages found for sending")
      }
    }
  }

  def bidiFlow: BidiFlow[ByteString, ByteString, ByteString, ByteString, NotUsed] = {

    BidiFlow.fromFunctions((chunk: ByteString) => {
      logger.info(s"Random Answer Server responded")
      //Future(Some(chunk.utf8String)) pipeTo sender//TODO can be doesn't full answer
      val decoded: Option[String] = MLLP.decode(chunk)
      if(decoded.isDefined){
        val mess = HL7Mess.decode(decoded.get);
        val msh = mess.mshSegment()
        val hl7Id = if(msh.isDefined) msh.get.messageControlID() else ""
        if(mess.errSegment().isDefined){
          logger.info(s"with ERROR MESSAGE HL7 ID: $hl7Id")
        }else {
          logger.info(s"with SUCCESS MESSAGE HL7 ID: $hl7Id")

          repository.updateAsSent(hl7Id).onComplete{
            case Success(_) => logger.info(s"Message with HL7 ID: $hl7Id updated in DB." )
            case _ => logger.info("Haven't updated.")
          }

        }
      }
      chunk
    },(c:ByteString) => {/*logger.info(s"Request: ${c.utf8String}");*/c})
  }

  def tlsClientLayer = {

    val tls = TLS(SSLContext.getDefault, NegotiateNewSession.withDefaults, TLSRole.client)

    val tlsSupport = BidiFlow.fromFlows(
      Flow[ByteString].map(TLSProtocol.SendBytes),
      Flow[TLSProtocol.SslTlsInbound].collect {
        case TLSProtocol.SessionBytes(_, sb) => ByteString.fromByteBuffer(sb.asByteBuffer)
      })

    tlsSupport.atop(tls)
  }

}

object ScheduleActor {
  val CHECK_MESSAGE = "check_message"
}
