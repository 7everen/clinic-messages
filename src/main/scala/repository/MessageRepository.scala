package repository

import model.database.{Hl7Message, Hl7MessageSchema}
import scalikejdbc._

import scala.concurrent.{ExecutionContext, Future}

class MessageRepository(session: DBSession, executionContext: ExecutionContext) {

  implicit val s = session
  implicit val ec = executionContext

  val m = Hl7MessageSchema.syntax("m")

  def findById(id: Long): Future[Option[Hl7Message]] = Future {
    DB readOnly { implicit s =>
      withSQL {
        select
          .from(Hl7MessageSchema as m)
          .where
          .eq(m.id, id)
          .limit(1)
      }.map(Hl7MessageSchema(m.resultName)).single.apply()
    }
  }

  /*def findByText(like: String): Future[Option[Hl7Message]] = Future {
    DB readOnly { implicit s =>
      sql"select * from db where name like '%|$like|%'".map(Hl7MessageSchema(m.resultName)).single.apply()
    }
  }*/

  def findFirstDidntSend(): Future[Option[Hl7Message]] = Future {
    DB readOnly { implicit s =>
      withSQL {
        select
          .from(Hl7MessageSchema as m)
          .where
          .eq(m.sent, false)
          .limit(1)
      }.map(Hl7MessageSchema(m.resultName)).single.apply()
    }
  }

  def findDidntSend(): Future[List[Hl7Message]] = Future {
    DB readOnly { implicit s =>
      withSQL {
        select
          .from(Hl7MessageSchema as m)
          .where
          .eq(m.sent, false)
      }.map(Hl7MessageSchema(m.resultName)).list().apply()
    }
  }

  def findSent(): Future[List[Hl7Message]] = Future {
    DB readOnly { implicit s =>
      withSQL {
        select
          .from(Hl7MessageSchema as m)
          .where
          .eq(m.sent, true)
      }.map(Hl7MessageSchema(m.resultName)).list().apply()
    }
  }

  val columns = Hl7MessageSchema.column

  def updateAsSent(id: Long): Future[Long] = Future {
    withSQL {
      update(Hl7MessageSchema).set(columns.sent -> true).where.eq(columns.id, id)
    }.update.apply()
  }

  def updateAsSent(like: String): Future[Long] = Future {
    withSQL {
      update(Hl7MessageSchema).set(columns.sent -> true).where.like(columns.text, s"%|$like|%")
    }.update.apply()
  }

  def updateAsNotSent(id: Long): Future[Long] = Future {
    withSQL {
      update(Hl7MessageSchema).set(columns.sent -> false).where.eq(columns.id, id)
    }.update.apply()
  }

}
