package endpoints

import java.lang.System.currentTimeMillis

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{pathPrefix, _}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server.directives.{DebuggingDirectives, LogEntry, LoggingMagnet}
import akka.http.scaladsl.settings.RoutingSettings
import akka.stream.{ActorMaterializer, Materializer}
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import model.database.Message
import repository.MessageRepository

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class Endpoints(repository: MessageRepository)(implicit ec: ExecutionContext, mat: Materializer)  {

  val messageRoute: Route = {

    pathPrefix("api" / "message") {
      (get & path(LongNumber)) { id =>
        onComplete(repository.findById(id)) {
          case Success(Some(message)) =>
            complete(Marshal(message).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(None)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "messageDidntSend") {
      get {
        onComplete(repository.findFirstDidntSend()) {
          case Success(Some(message)) =>
            complete(Marshal(message).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(None)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "countDidntSend") {
      get {
        onComplete(repository.findDidntSend()) {
          case Success(list) =>
            complete(Marshal(list.size).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "messagesDidntSend") {
      get {
        onComplete(repository.findDidntSend()) {
          case Success(list) =>
            complete(Marshal(list).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "countDidntSend") {
      get {
        onComplete(repository.findDidntSend()) {
          case Success(list) =>
            complete(Marshal(list.size).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "messagesDidntSend") {
      get {
        onComplete(repository.findDidntSend()) {
          case Success(list) =>
            complete(Marshal(list).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "countSent") {
      get {
        onComplete(repository.findSent()) {
          case Success(list) =>
            complete(Marshal(list.size).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "messagesSent") {
      get {
        onComplete(repository.findSent()) {
          case Success(list) =>
            complete(Marshal(list).to[ResponseEntity].map { e => HttpResponse(entity = e) })
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.NotFound))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    } ~ pathPrefix("api" / "updateMessageToNotSent") {
      (get & path(LongNumber)) { id =>
        onComplete(repository.updateAsNotSent(id)) {
          case Success(_)       =>
            complete(HttpResponse(status = StatusCodes.OK))
          case Failure(e)          =>
            complete(Marshal(Message(e.getMessage))
              .to[ResponseEntity].map { e =>
              HttpResponse(entity = e, status = StatusCodes.InternalServerError) })
        }
      }
    }

  }

  def routes(implicit
             sys: ActorSystem,
             mat: ActorMaterializer,
             ec: ExecutionContext) = loggableRoute {
    Route.seal {
      messageRoute
    }
  }

  def logRequestAndResponse(loggingAdapter: LoggingAdapter, before: Long)(req: HttpRequest)(res: Any): Unit = {
    val entry = res match {
      case Complete(resp) =>
        val message = s"{path=${req.uri}, method=${req.method.value}, status=${resp.status.intValue()}, elapsedTime=${currentTimeMillis() - before}"
        LogEntry(message, Logging.InfoLevel)
      case other => LogEntry(other, Logging.InfoLevel)
    }

    entry.logTo(loggingAdapter)
  }

  def loggableRoute(route: Route)(implicit m: Materializer,
                                  ex: ExecutionContext,
                                  routingSettings: RoutingSettings): Route = {
    DebuggingDirectives.logRequestResult(LoggingMagnet(log => {
      val requestTimestamp = currentTimeMillis()
      logRequestAndResponse(log, requestTimestamp)
    }))(route)
  }

}
