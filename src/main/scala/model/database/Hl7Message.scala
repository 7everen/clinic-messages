package model.database

import io.circe._
import io.circe.syntax._
import scalikejdbc._

case class Hl7Message(id: Long, text: String, sent:Boolean) {
  require(text != null, "text not informed")
  require(text.nonEmpty && text.length < 2048, "text cannot be empty and more than 2048 chars")
}

object Hl7MessageSchema extends SQLSyntaxSupport[Hl7Message] {
  override val tableName = "messages"

  def apply(m: ResultName[Hl7Message])(rs: WrappedResultSet): Hl7Message = {
    Hl7Message(rs.long(m.id), rs.string(m.text), rs.boolean(m.sent))
  }
}

object Hl7Message {
  implicit val encoder: Encoder[Hl7Message] = (a: Hl7Message) => {
    Json.obj(
      "id" -> a.id.asJson,
      "text" -> a.text.asJson,
      "sent" -> a.sent.asJson
    )
  }

  implicit val decoder: Decoder[Hl7Message] = (c: HCursor) => {
    for {
      text <- c.downField("text").as[String]
      sent <- c.downField("sent").as[Boolean]
    } yield Hl7Message(-1, text, sent)
  }
}

case class Message(message: String)

object Message {
  implicit val encoder: Encoder[Message] = m => Json.obj("message" -> m.message.asJson)
}

