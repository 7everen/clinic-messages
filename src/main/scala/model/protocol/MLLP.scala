package model.protocol

import java.nio.ByteOrder

import akka.util.{ByteString, ByteStringBuilder}

class MLLP

object MLLP {
  implicit val byteOrder = ByteOrder.BIG_ENDIAN

  def encode(message:String): ByteString = (new ByteStringBuilder).putByte(11).append(ByteString(message)).putByte(12).putByte(13).result()

  def decode(data:ByteString): Option[String] = {
    val end = data.indexOf(0x0C)
    if(data.head != 0x0B || end == -1) None else Some(new String(data.slice(1, end).utf8String))
  }

}
