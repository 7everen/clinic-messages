package model.protocol

/**
  * HL7Composite
  */

case class HL7Composite(val subComposites:String)

object HL7Composite{

  private val delimiter = '^'

  def decode(data:String):HL7Composite = HL7Composite(data)

  def encode(composite:HL7Composite):String = composite.subComposites

}

/**
  * HL7Segment
  */

sealed trait HL7SegmentType { def name: String }
case object MSH extends HL7SegmentType { val name = "MSH" }
case object PID extends HL7SegmentType { val name = "PID" }
case object NK1 extends HL7SegmentType { val name = "NK1" }
case object PV1 extends HL7SegmentType { val name = "PV1" }
case object ERR extends HL7SegmentType { val name = "ERR" }

case class HL7UnknownSegmentType(name: String) extends HL7SegmentType

trait BaseHL7Segment {
  val segmentType:HL7SegmentType
  val composites:List[HL7Composite]
}

case class HL7MSHSegment(val composites:List[HL7Composite]) extends BaseHL7Segment {

  val segmentType:HL7SegmentType = MSH

  def messageControlID():String = if(composites.length>8) composites(8).subComposites else ""

}

case class HL7ERRSegment(val composites:List[HL7Composite]) extends BaseHL7Segment {

  val segmentType:HL7SegmentType = ERR

}

case class HL7Segment(val segmentType:HL7SegmentType, val composites:List[HL7Composite]) extends BaseHL7Segment

object HL7Segment{

  private val delimiter = '|'

  def decode(data:String):BaseHL7Segment = {
    val splitted = data.split(delimiter).toList
    val composites:List[HL7Composite] = splitted.tail.map(HL7Composite.decode(_))

    return splitted.head match {
      case MSH.name => HL7MSHSegment(composites)
      case ERR.name => HL7ERRSegment(composites)
      case a:String => HL7Segment(HL7UnknownSegmentType(a), composites)
    }
  }

  def encode(segment:BaseHL7Segment):String = segment.composites.map(c => HL7Composite.encode(c)).foldLeft(segment.segmentType.name)(_ + delimiter + _)

}

/**
  * HL7Mess
  */

case class HL7Mess(val segments:List[BaseHL7Segment]){

  def mshSegment():Option[HL7MSHSegment] = segments.find(o => o.isInstanceOf[HL7MSHSegment]).asInstanceOf[Option[HL7MSHSegment]]

  def errSegment():Option[HL7ERRSegment] = segments.find(o => o.isInstanceOf[HL7ERRSegment]).asInstanceOf[Option[HL7ERRSegment]]

}

object HL7Mess {

  private val delimiter = '\n'

  def decode(data:String):HL7Mess = HL7Mess(data
      .split(delimiter)
      .filter(!_.isEmpty)
      .map(HL7Segment.decode(_)).toList)

  def encode(message:HL7Mess):String = message.segments.map(s => HL7Segment.encode(s)).mkString(delimiter.toString)


}