# clinic-messages

This example shows a simple microservice written in Scala using:

- [Akka HTTP](https://doc.akka.io/docs/akka-http/current/index.html?language=scala), to expose the http services
- [ScalikeJDBC](http://scalikejdbc.org/), to access PostgreSQL using a Scala JDBC wrapper
- [Akka STREAM](https://doc.akka.io/docs/akka/current/stream/index.html?language=scala), to expose the stream services  
- [Akka SLF4J](https://doc.akka.io/docs/akka/2.5/logging.html#slf4j-directly) to able a specific logging backend 
- [MLLP Protocol](http://www.hl7.org/documentcenter/public/wg/inm/mllp_transport_specification.PDF) the Minimal Lower Layer protocol (MLLP, a.k.a. MLP). The MLLP protocol has a long history of use within the HL7 community, although it has never been formally part of the HL7 standard itself.
- [HL7 Message](https://blog.interfaceware.com/understanding-hl7-messages/) are used to transfer electronic data between disparate healthcare systems.

This service includes:
 - main application(main class: MainServer)
 - random answer application(main class is MLLPRandomAnswerServer)
 
 Working version, need to be improve...
 
 #### Note on configuration
 
 Config file have 3 main groups for
  - DataBase 
```
  jdbc {
    url = "jdbc:postgresql://localhost:5432/akka"
    username = "root"
    password = "root"
    maxConnections = 10
    driver = "org.postgresql.Driver"
  }
```
  - main application 
```
    mainServer{
      host: "localhost"
      port: 8080
      scheduleTimeRepeat:10  //seconds
    }
```
    
  - random asnwer application
```
  randomAnswerServer{
    host: "localhost"
    port: 8088
    tls: false
  }
```
  #### API requests
  
  show message by id:
```
GET localhost[:port]/api/message/[id]
```
  next message for sending:
```
GET localhost[:port]/api/messageDidntSend
```
All messages need to be send:
```
GET localhost[:port]/api/messagesDidntSend
```
Count of messages need to be send:
```
GET localhost[:port]/api/countDidntSend
```
All messages already sent:
```
GET localhost[:port]/api/messagesSent
```
Count of messages already sent:
```
GET localhost[:port]/api/countSent
```
Update message to not sent:
```
GET localhost[:port]/api/updateMessageToNotSent/[id]
```

 #### SQL 
 
 I added sql_export.sql file with table and data
 
 #### Tests
 
 Also I added couple tests in HL7MessageSpec, that checks correct decode/encode MLLP data and HL7 Messages 
 
 
 

