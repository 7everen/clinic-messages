name := "clinic-messages"

version := "0.1"

scalaVersion := "2.12.6"

val akkaHttp = "10.1.1"
val akka = "2.5.11"
val scalikejdbc = "3.2.2"
val circe = "0.9.3"
val postgre = "42.1.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttp,
  "com.typesafe" % "config" % "1.3.2",
  "com.typesafe.akka" %% "akka-stream" % akka,
  "com.typesafe.akka" %% "akka-slf4j" % akka,
  "org.postgresql" % "postgresql" % postgre,
  "org.scalikejdbc" %% "scalikejdbc" % scalikejdbc,

  "de.heikoseeberger" %% "akka-http-circe" % "1.20.1",
  "io.circe" %% "circe-generic" % circe,

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "org.scalatest" %% "scalatest" % "3.0.1" % "test"

)

testOptions in Test ++= Seq(
  Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/test-reports"),
  Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports")
)